### 使用说明
1. 本项目是为了学习  [APICloud](http://www.apicloud.com)  APP开发，没有任何其他目的和想法。请测试完成后24小时内删除，切勿非法使用。所以系统也不自动提供最新地址，还望谅解！
2. **系统内置的是被屏蔽的官网网址，在测试前请自己去找无需 代理/VPN 的地址，然后在软件内自定义！**

### 开发说明
项目是基于[APICloud](http://www.apicloud.com) 的，详细的开发文档请看[官网文档](http://docs.apicloud.com/APICloud/creating-first-app)

### 打包编译
1. [APICloud](http://www.apicloud.com) 注册帐号
2. 下载IDE
3. 创建项目
4. 复制代码到你的新项目内（config.xml就不用复制了）
5. 然后IDE内右键项目，云端同步-提交
6. 最后去官网云编译（注意：云编译前需要添加 imageBrowser、chatBox [模块](http://www.apicloud.com/module-store)）。当然IDE也可以打包的

### 


### 扫描下载
![安卓下载](http://git.oschina.net/uploads/images/2015/0715/005355_6160fd66_431.jpeg "安卓下载")![iPhone下载](http://git.oschina.net/uploads/images/2015/0715/005417_9879d34b_431.jpeg "iPhone下载")

对应版本：2015-07-15 d403435ae

### 预览图
![图1](http://static.oschina.net/uploads/space/2015/0417/200716_XWs8_252582.jpg "图1")

![图2](http://static.oschina.net/uploads/space/2015/0417/200716_pJpo_252582.jpg "图2")

![图3](http://static.oschina.net/uploads/space/2015/0409/031334_SspJ_252582.jpg "图3")

![图4](http://static.oschina.net/uploads/space/2015/0417/200717_suhe_252582.jpg "图4")

![图5](http://static.oschina.net/uploads/space/2015/0417/200717_MRWf_252582.jpg "图5")

![图6](http://static.oschina.net/uploads/space/2015/0409/144755_64CJ_252582.jpg "图6")