/*
 * 向控制台输出日志
 * @param  data data  要输出的日志
 * @return string
 */
function log(data){
	if(typeof data == 'string'){
		console.log(data);
	}else{
		console.log(JSON.stringify(data));
	}
}

function showErrMgs(code, msg){
	switch( parseInt(code) )
	{
	case 0:
		msg = "网络无法连接\n请检查网络配置或自定义网址已失效";
	  break;
	case 1:
	 	msg = "网络请求超时\n可能自定义网址已失效";
	  break;
	}
    
	api.toast({
	    msg: msg,
	    duration:2000,
	    location: 'middle'
	});
}

function htmlcompress(source){
	if(source.length==0){
		return source;
	}
    var rep = /\n+/g;
    var repone = /<!--.*?-->/ig;
    var reptwo = /\/\*.*?\*\//ig;
    var reptree = /[ ]+</ig;
    var sourceZero = source.replace(rep,"");
    var sourceOne = sourceZero.replace(repone,"");
    var sourceTwo = sourceOne.replace(reptwo,"");
    var sourceTree = sourceTwo.replace(reptree,"<");
    return sourceTree;
}

// 返回子窗口的顶部
function gotop(frameName){
	api.execScript({
	    frameName: frameName,
	    script: "$('body').scrollTop(0);"
	});
}

/*
 * 时间转换时间戳 
 * @param  string endTime  日期 2015-03-29 20:21:32
 * @return int	时间戳 
 */
function transdate(endTime){
	var date=new Date();
	date.setFullYear(endTime.substring(0,4));
	date.setMonth(endTime.substring(5,7)-1);
	date.setDate(endTime.substring(8,10));
	date.setHours(endTime.substring(11,13));
	date.setMinutes(endTime.substring(14,16));
	date.setSeconds(endTime.substring(17,19));
	return Date.parse(date)/1000;
}

/*
 * 显示友好时间
 * @param  string date  日期 2015-03-29 20:21:32
 * @return string	友好的时间 
 */
function friendly_time(date)
{
	var time_stamp = transdate(date)
    var now_d = new Date();
    var now_time = now_d.getTime() / 1000; //获取当前时间的秒数
    var f_d = new Date();
    f_d.setTime(time_stamp * 1000);
    var f_time = f_d.toLocaleDateString();

    var ct = now_time - time_stamp;
    var day = 0;
    if (ct < 0)
    {
        f_time = "【预约】" + f_d.toLocaleString();
    }
    else if (ct < 60)
    {
        f_time = Math.floor(ct) + '秒前';
    }
    else if (ct < 3600)
    {
        f_time = Math.floor(ct / 60) + '分钟前';
    }
    else if (ct < 86400)//一天
    {
        f_time = Math.floor(ct / 3600) + '小时前';
    }
    else if (ct < 691200)//7天
    {
        day = Math.floor(ct / 86400);
        if (day < 2)
            f_time = '1天前';
        else
            f_time = day + '天前';
    }
    else if(ct >= 691200)
    {
        f_time=date.substring(0, date.length-9);
    }
    return f_time;
}

/*
 * 获取用户基本信息
 * @param  string site  网址
 */
function getUserInfo(site){
	api.showProgress({
	    style: 'default',
	    animationType: 'fade',
	    title: '获取用户信息中',
	    modal: false
	});
	
	api.ajax({
	    url: site+'/profile.php',
	    method: 'get',
	    timeout: 30,
	    dataType: 'text',
	    charset: 'gbk',
	},function(ret,err){
		api.hideProgress();
		var rgexp = /您還沒有登錄或註冊/;
	    if (rgexp.test(ret)) {
	    	$api.rmStorage('uid');
	    	$api.rmStorage('username');
	    	$api.rmStorage('groupname');
	    }else{
			var mUid = ret.match(/profile.php\?action=show&uid=(\d+)/);
			if("undefined" == typeof mUid){
				return false;
			}
			var uid = mUid[1];	// 用户ID
	    	$api.setStorage('uid', uid);

			var mUsername = ret.match(/用戶名: (.+)/);
			var username = mUsername[1];	// 用户名
	    	$api.setStorage('username', username);
	    	
			var mGroupname = ret.match(/會員頭銜: (.+)<br/);
			var groupname = mGroupname[1];	// 頭銜
	    	$api.setStorage('groupname', groupname);
	    }
	    
		api.execScript({
		    name: 'fixed',
		    script: 'setUserInfo();'
		});
	    
	});
}