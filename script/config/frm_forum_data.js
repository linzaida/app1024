var forum_data = [{
    "catname": "»草榴休閑區",
    "forum": [{
        "fid": 7,
        "fname": "技術討論區",
        "fdes": "日常生活 興趣交流 時事經濟 求助求檔 會員閑談吹水區",
        "fico": "../image/forum/1.png"
    },
    {
        "fid": 8,
        "fname": "新時代的我們",
        "fdes": "草榴貼圖區 加大你的帶寬! 加大你的內存! 加大你的顯示器! ",
        "fico": "../image/forum/2.png"
    },
    {
        "fid": 16,
        "fname": "達蓋爾的旗幟",
        "fdes": "草榴自拍區 分享你我光圈下的最美",
        "fico": "../image/forum/3.png"
    },
    {
        "fid": 20,
        "fname": "成人文學交流區",
        "fdes": "草榴文學區 歡迎各位發表",
        "fico": "../image/forum/4.png"
    },
    {
        "fid": 9,
        "fname": "草榴資訊",
        "fdes": "公告有關本站最新動向 會員須知 請經常來看看",
        "fico": "../image/forum/5.png"
    }]
},
{
    "catname": "»BT電影下載",
    "forum": [{
        "fid": 2,
        "fname": "亞洲無碼原創區",
        "fdes": "自由發布亞洲最新無修正資訊片 亞洲無碼V大聯盟",
        "fico": "../image/forum/6.png"
    },
    {
        "fid": 15,
        "fname": "亞洲有碼原創區",
        "fdes": "自由發布亞洲最新有修正資訊片 以及三級等其他資訊片 ",
        "fico": "../image/forum/7.png"
    },
    {
        "fid": 4,
        "fname": "歐美原創區",
        "fdes": "自由發布純正的歐美成人資訊片",
        "fico": "../image/forum/8.png"
    },
    {
        "fid": 5,
        "fname": "動漫原創區",
        "fdes": "自由發布任何H動畫漫畫",
        "fico": "../image/forum/9.png"
    },
    {
        "fid": 21,
        "fname": "HTTP下載區",
        "fdes": "自由發布各類HTTP/Ray/eMule等方式下載",
        "fico": "../image/forum/10.png"
    },
    {
        "fid": 22,
        "fname": "在綫成人影院",
        "fdes": "在綫欣賞,即點即看",
        "fico": "../image/forum/11.png"
    },
    {
        "fid": 10,
        "fname": "草榴影視庫",
        "fdes": "發布-3個月后的帖子,會轉移到這里,保留一段時間后,刪除(會員可看)",
        "fico": "../image/forum/12.png"
    }]
}];